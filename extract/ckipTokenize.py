from snownlp import SnowNLP
from collections import Counter
from ckip_transformers.nlp import CkipWordSegmenter, CkipPosTagger, CkipNerChunker
from dataclasses import dataclass

from .allowPOS import allowPOS

level = 1

ws = CkipWordSegmenter(level=level)
pos = CkipPosTagger(level=level)
ner = CkipNerChunker(level=level)


@dataclass
class myTokenizer:
    topFreq: int = 100

    def fit(self, context):
        self.context = context
        self._tokenize_tokens()
        self._tokenize_pos()
        self._tokenize_entity_list()
        self._tokenize_word_pos_pair()
        self._tokenize_tokens_v2()
        self._cal_keyfreqs()
        self._snowNLP()

        return {
            'tokens': self.tokens,
            'tokens_v2': self.tokens_v2,
            'entities': self.entity_list,
            'token_pos': self.tokens_pos,
            'top_key_freq': self.keyfreqs,
            'summary': self.summary,
            'sentiment': self.sentiment
        }
        
    # Tokenize
    def _tokenize_tokens(self):
        tokens = ws([self.context])
        self.tokens = [[e] for e in tokens[0]]


    def _tokenize_tokens_v2(self):
        tokens_v2 = []
        for wp in self.word_pos_pair:
            tokens_v2.append([w for w, p in wp if (len(w) >= 2) and p in allowPOS.tokens_v2])
        
        while [] in tokens_v2:
            tokens_v2.remove([])

        self.tokens_v2 = tokens_v2
        
    
    def _tokenize_pos(self):
        self.tokens_pos = pos(self.tokens)
    

    def _tokenize_word_pos_pair(self):
        self.word_pos_pair = [list(zip(w, p)) for w, p in zip(self.tokens, self.tokens_pos)]


    def _tokenize_entity_list(self):

        entity_list = ner([self.context])
        print(entity_list)
        while [] in entity_list:
            entity_list.remove([])

        entity_list = ''.join(str(e) for e in entity_list)
        self.entity_list = entity_list
    

    # Cal Frequency
    def _cal_word_frequency(self, wp):
        # filtered_words = []
        
        for word, pos in wp:
            if (pos in allowPOS.word_frequency) & (len(word) >= 2):
                return word
                
                # filtered_words.append(word)
        # counter = Counter(filtered_words)
        # return counter.most_common(self.topFreq)
    
    def _cal_keyfreqs(self):
        freqs = []
        NoneType = type(None)
        for wp in self.word_pos_pair:
            word = self._cal_word_frequency(wp)
            if type(word) != NoneType:
                freqs.append(word)
        
        counter = Counter(freqs)
        # while [] in keyfreqs:
        #     keyfreqs.remove([])
        
        self.keyfreqs = counter.most_common(self.topFreq)
    
    # snowNLP
    def _snowNLP(self):
        sn = SnowNLP(self.context)
        self.summary = sn.summary()
        self.sentiment = round(sn.sentiments, 2)
        

