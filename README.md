<div align='center'>
  <img width='25%' src='https://cdn-icons-png.flaticon.com/512/2504/2504138.png'>

# **Anue 鉅亨網**
[**@hsiangjenli**](https://gitlab.com/hsiangjenli)
</div>

## **Clone**
```shell
git clone https://gitlab.com/tuxedo-web-scraping/anue.git
```
## **Usage**
```python
from anue import Anue
```
```python
anue_json = Anue(category='tw_stock_news', period=1) # Return Json

for news in anue_json.crawler():
    print(news)
    break
```
```python
anue_df = Anue(category='tw_stock_news', period=1, dataframe=True) # Return Dataframe

for news in anue_df.crawler():
    break
news
```
## **pymongo**
```python
import pymongo

collectionName = 'TEST'
dbName = 'DB_TEST'
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient[dbName] # DB Name
mycol = mydb[collectionName] # Collection Name
```
```python
from anue import Anue

anue = Anue(category='tw_stock_news', period=1)
for data in anue.crawler():
    mycol.insert_one(data)
    # break
```
![Image](https://i.imgur.com/U3Rh8hX.png)

## **Tokenize**
```python
from anue import Anue

anue_json = Anue(category='tw_stock_news', period=1)

for news in anue_json.crawler():
    print(news)
    break
```
```python
outputs = mytokenizer.fit(news['Context'])

# outputs.keys()
# dict_keys(['tokens', 'tokens_v2', 'entities', 'token_pos', 'top_key_freq', 'summary', 'sentiment'])
```

---
<small>

> <a href="https://www.flaticon.com/free-icons/web-crawler" title="web crawler icons">Web crawler icons created by mynamepong - Flaticon</a>
</small>
