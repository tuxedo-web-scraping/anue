import os, re
import datetime

from tokenize import String
import requests, json
from bs4 import BeautifulSoup
from dataclasses import dataclass
import datetime, time
import pandas as pd

class ANUE_URL:

    period = 'startAt={t1}&endAt={t2}&limit=30&page={page}'
    v3 = 'https://news.cnyes.com/api/v3/news/category/{cat}?' + period
    anue_news = 'https://news.cnyes.com/news/id/{newsId}?exp=a'

@dataclass
class Anue:
    period: int
    category: String
    sleep: int = 3
    dataframe: bool = False

    def crawler(self):
        api_first_page_url = self.api_url(page=1)
        pages = self.page_num(rawJson=self.get_api_json(url=api_first_page_url))
        newsIds = self.get_all_newsId(pages=pages)
        for nid in newsIds:
            url = ANUE_URL.anue_news.format(newsId=nid)
            r = requests.get(url)
            self.soup = BeautifulSoup(r.text, 'lxml')

            title = self.find_title()
            context = self.find_context()
            publishDate = self.find_publishDate()
            imgUrl = self.find_imgUrl()
            tags = self.find_tags()

            rData = {
                'Category': self.category,
                'newsId': nid,
                'Date': publishDate,
                'Title': title,
                'Tags': tags,
                'Context': context,
                'img_url': imgUrl,
                'news_url': url
            }

            if self.dataframe:
                yield pd.DataFrame(

                    [ list(rData.values()) ], 

                    columns=rData.keys()
                )
            else:
                yield rData
                
            time.sleep(self.sleep)


    def api_url(self, page):
        self.t1, self.t2 = self.period_to_timestamp(self.period)
        url = ANUE_URL.v3.format(cat = self.category, t1 = self.t1, t2 = self.t2, page = page)
        return url
    
    def get_api_json(self, url):
        r = requests.get(url)
        data = json.loads(r.text)
        return data

    def page_num(self, rawJson):
        return rawJson['items']['last_page'] + 1
    
    def get_all_newsId(self, pages):
        newsIds = []
        for page in range(1, pages):
            url = ANUE_URL.v3.format(cat = self.category, t1 = self.t1, t2 = self.t2, page = page)
            rawJson = self.get_api_json(url=url)
            for data in rawJson['items']['data']:
                newsIds.append(data['newsId'])
        return newsIds

    def period_to_timestamp(self, period):

        end = datetime.datetime.now()
        start = end - datetime.timedelta(days = period)

        end = round(datetime.datetime.timestamp(end))
        start = round(datetime.datetime.timestamp(start))
        
        return [start, end]
    
    def find_title(self):
        return self.soup.find('h1').text
    def find_context(self):
        return self.soup.find('div', attrs={'class': '_2E8y'}).text.replace('\n', '')
    def find_publishDate(self):
        return datetime.datetime.strptime(self.soup.find('time').text, '%Y/%m/%d %H:%M')
    def find_imgUrl(self):
        try:
            return self.soup.find('figure').img['src']
        except:
            return ''
    def find_tags(self):
        raw = self.soup.find_all('span', attrs={'class': '_1E-R'})
        rawtags = [tag.text for tag in raw]
        tags = [tag for tag in rawtags if tag]
        return tags
