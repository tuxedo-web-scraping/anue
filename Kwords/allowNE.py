from dataclasses import dataclass, field
from typing import List
import pandas as pd
from collections import Counter


class allowNE:
    person = ['PERSON']

def NerToken(word, ner, idx):
    return ner,word


@dataclass
class calNEFrequency:
    cats: List[str] = field(default_factory=list)
    df: pd.DataFrame = None
    topFreq: int = 20

    def run(self, allowNE):
        outputs = self._cal_each_category(allowNE=allowNE)
        return outputs

    def _cal_each_category(self, allowNE):
        outputs = {}

        # Each Category
        for cat in self.cats:
            allNE = []
            df_cat = self.df[self.df.Category==cat]
            for ne in df_cat.entities:
                allNE += eval(ne)
            outputs[cat] = self._cal_allNE_Frequency(allNE=allNE, allowNE=allowNE)
        
        # All Category
        allNE = []
        for ne in self.df.entities:
            allNE += eval(ne)
        outputs['all'] = self._cal_allNE_Frequency(allNE=allNE, allowNE=allowNE)

        return outputs
    
    def _cal_allNE_Frequency(self, allNE, allowNE):
        filtered_words =[]
        for ner, word in allNE:
            if (len(word) >= 2) & (ner in allowNE):
                filtered_words.append(word)
        
        counter = Counter( filtered_words )
        return counter.most_common(self.topFreq)


